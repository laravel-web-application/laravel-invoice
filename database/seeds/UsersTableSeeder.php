<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Uchiha Itachi',
            'email' => 'support@konohagakure.co.jp',
            'password' => bcrypt('secret')
        ]);
    }
}
