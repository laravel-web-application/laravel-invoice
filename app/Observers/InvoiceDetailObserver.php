<?php

namespace App\Observers;

use App\InvoiceDetail;

class InvoiceDetailObserver
{
    /**
     * Handle the invoice detail "created" event.
     *
     * @param \App\InvoiceDetail $invoiceDetail
     * @return void
     */
    public function created(InvoiceDetail $invoiceDetail)
    {
        //PANGGIL METHOD BARU TERSEBUT
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice detail "updated" event.
     *
     * @param \App\InvoiceDetail $invoiceDetail
     * @return void
     */
    public function updated(InvoiceDetail $invoiceDetail)
    {
        //PANGGIL METHOD BARU TERSEBUT
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice detail "deleted" event.
     *
     * @param \App\InvoiceDetail $invoiceDetail
     * @return void
     */
    public function deleted(InvoiceDetail $invoiceDetail)
    {
        //PANGGIL METHOD BARU TERSEBUT
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice detail "restored" event.
     *
     * @param \App\InvoiceDetail $invoiceDetail
     * @return void
     */
    public function restored(InvoiceDetail $invoiceDetail)
    {
        //
    }

    /**
     * Handle the invoice detail "force deleted" event.
     *
     * @param \App\InvoiceDetail $invoiceDetail
     * @return void
     */
    public function forceDeleted(InvoiceDetail $invoiceDetail)
    {
        //
    }

    //KARENA FUNGSI YANG DIJALANKAN SAMA, MAKA KITA MEMBUATNYA KEDALAM FUNGCTION BARU
    private function generateTotal($invoiceDetail)
    {
        //MENGAMBIL INVOICE_ID
        $invoice_id = $invoiceDetail->invoice_id;
        //SELECT DARI TABLE invoice_details BERDASARKAN INVOICE
        $invoice_detail = InvoiceDetail::where('invoice_id', $invoice_id)->get();
        //KEMUDIAN DIJUMLAH UNTUK MENDAPATKAN TOTALNYA
        $total = $invoice_detail->sum(function ($i) {
            //DIMANA KETENTUAN YANG DIJUMLAHKAN ADALAH HASIL DARI price* qty
            return $i->price * $i->qty;
        });
        //UPDATE TABLE invoice PADA FIELD TOTAL
        $invoiceDetail->invoice()->update([
            'total' => $total
        ]);
    }


}
