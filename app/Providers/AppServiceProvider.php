<?php

namespace App\Providers;

use App\InvoiceDetail;
use App\Observers\InvoiceDetailObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //DEFINE OBSERVER YANG TELAH DIBUAT
        //Invoce_detail adalah nama class dari model
        //Invoice_detailObserver adalah nama class dari observer itu sendiri
        InvoiceDetail::observe(InvoiceDetailObserver::class);
    }
}
